package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
public class ShowTwoNumbers {
    public static void main(String[] args) {
        String str1, str2;
        String strNotify = "Yoy've just entered:  ";
        
        str1 = JOptionPane.showInputDialog(null, 
        "Please enter the 1st number: ", "Enter the 1st number: ",
        JOptionPane.INFORMATION_MESSAGE);
        strNotify += str1 + " and ";
        str2 = JOptionPane.showInputDialog(null, 
        "Please enter the 2nd number: ", "Enter the 2nd number: ",
        JOptionPane.INFORMATION_MESSAGE);
        strNotify += str2;
        JOptionPane.showMessageDialog(null, strNotify, "Show 2 numbers: ", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
