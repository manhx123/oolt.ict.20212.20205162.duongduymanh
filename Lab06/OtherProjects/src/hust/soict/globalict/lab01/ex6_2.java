package hust.soict.globalict.lab01;

import java.util.Scanner;
public class ex6_2 {
    double a1, b1, c1;
    double a2, b2, c2;
    public void input(){
        Scanner s = new Scanner(System.in);
        System.out.println("------------SSG-----------");
        System.out.println("Vao he so: a1, b1, c1 = ");
        a1 = s.nextDouble();       b1 = s.nextDouble();  c1 = s.nextDouble();
        System.out.println("--Het PT1. Moi ban Nhap PT2--");
        System.out.println("a2, b2, c2 = ");
        a2 = s.nextDouble();   b2 = s.nextDouble();        c2 = s.nextDouble();
        System.out.println("-------Hoan thanh Nhap-------");
        System.out.println("");
        s.close();
    }
   public void PpThe(){
    double b11=b1*a2;        
    double c11=c1*a2;       
    double b22=b2*a1;        
    double c22=c2*a1;
    
    double y = (c22-c11)/(b11-b22);
    System.out.println("y = "+y);
    double x = (-c11-b11*y)/(a1*a2);
    System.out.println("x = "+x);
   }
   public class THUATTOAN{
    public static Double[] DinhThuc(ex6_2 pt) {
        Double D[] = new Double[3];
        D[0] = (pt.a1 * pt.b2) - (pt.a2 * pt.b1);
        D[1] = ((-(pt.c1)) * pt.b2) - (pt.b1 * (-(pt.c2)));
        D[2] = (pt.a1 * (-(pt.c2))) - (pt.a2 * (-(pt.c1)));
        return D;
    }

    public static boolean checkPTVN(ex6_2 _pt) {
        boolean check = true;
        if (((_pt.a1 / _pt.a2) == (_pt.b1 / _pt.b2)) && ((_pt.c1 / _pt.c2) == (_pt.b1 / _pt.b2))) 
            check = false;
        if ((_pt.a1 ==0 && _pt.a2==0 )  || (_pt.a2 ==0 &&  _pt.b2 ==0))  check =false;
        double c = THUATTOAN.DinhThuc(_pt)[0];
        if (c==0) check=false;
        return check;
   }
   }
   public static void main(String[] args) {
    ex6_2 pt1 = new ex6_2();
    pt1.input();
    if (!THUATTOAN.checkPTVN(pt1))   {
        System.out.println("__TB: PT VO NGHIEM__");
    } else {
        System.out.println("-----NGHIEM THEO PP THE-------");
        pt1.PpThe();
        Double D[] = THUATTOAN.DinhThuc(pt1);
        System.out.println("----NGHIEM THEO DINH THUC-----");
        System.out.println("Phuong trinh co nghiem la:");
         System.out.println("y= " + D[2] / D[0]);
        System.out.println("x= " + D[1] / D[0]);
       
    }
   }
}
