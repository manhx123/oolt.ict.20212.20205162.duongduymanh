package hust.soict.globalict.garbage;
import java.io.*;
public class GarbageCreator {
    public static String readFile() {
        try {
            File file = new File(".");
            for (String fileNames : file.list())
                System.out.println(fileNames);
            BufferedReader br = new BufferedReader(new FileReader("file.txt"));

            String sb = "";
            String line = br.readLine();

            while (line != null) {
                sb += (line);
                sb += System.lineSeparator();
                line = br.readLine();
            }
            String everything = sb.toString();
            br.close();
            return everything;
        } catch (IOException err) {
            err.printStackTrace();
            return "Fail!";
        }
    }

    private GarbageCreator() {
    }
}
