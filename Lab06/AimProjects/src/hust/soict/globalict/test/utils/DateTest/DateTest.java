package hust.soict.globalict.test.utils.DateTest;

public class DateTest {
    public static void main(String[] args) {
        MyDate dateTest1 = new MyDate();
        dateTest1.print();

        System.out.println("Set the date to 17/8/1966");
        MyDate dateTest2 = new MyDate(17,8,1966);
        dateTest2.print();

        MyDate dateTest3 = new MyDate("2023-12-05");
        dateTest3.print();

        MyDate dateTest4 = new MyDate();
        dateTest4.accept();
        dateTest4.print();
    }
}
