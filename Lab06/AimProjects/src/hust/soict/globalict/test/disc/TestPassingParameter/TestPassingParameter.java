package hust.soict.globalict.test.disc.TestPassingParameter;

public class TestPassingParameter {
    public DigitalVideoDisc digitalVideoDisc;
    TestPassingParameter(DigitalVideoDisc dvd){
        this.digitalVideoDisc = dvd;
    }

    public static void main(String[] args) {
        DigitalVideoDisc jungle = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderella = new DigitalVideoDisc("Cinderella");
        TestPassingParameter jungleWrapper = new TestPassingParameter(jungle);
        TestPassingParameter cinderellaWrapper = new TestPassingParameter(cinderella);

        swap(jungleWrapper, cinderellaWrapper);
        System.out.println("Jungle DVD Title: " + jungleWrapper.digitalVideoDisc.getTitle());
        System.out.println("Cinderella DVD Title: " + cinderellaWrapper.digitalVideoDisc.getTitle());

        changeTitle(jungle,cinderella.getTitle());
        System.out.println("Jungle DVD Title: " + jungle.getTitle());
    }
    public static void swap(TestPassingParameter obj1, TestPassingParameter obj2){
        DigitalVideoDisc tmp = obj1.digitalVideoDisc;
        obj1.digitalVideoDisc = obj2.digitalVideoDisc;
        obj2.digitalVideoDisc = tmp;
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title){
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }
}
