package hust.soict.globalict.aims;
import hust.soict.globalict.aims.order.Order.Order;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
public class DiskTest {
    public static void main(String[] args) {
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Doctor Strange and The Multiverse of Madness");
        dvd1.setCost(50);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Batman: The Killing Joke");
        dvd2.setCost(32);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Elden Ring: The Fallen Grace");
        dvd3.setCost(56);

        Order newOrder = new Order();
        newOrder.addDigitalVideoDisc(dvd1, dvd2, dvd3);

        System.out.println(dvd1.search("Harry Potter"));
        System.out.println(dvd2.search("Wick 2"));
        System.out.println(newOrder.getALuckyItem().getTitle());
        newOrder.print();
    }
}
