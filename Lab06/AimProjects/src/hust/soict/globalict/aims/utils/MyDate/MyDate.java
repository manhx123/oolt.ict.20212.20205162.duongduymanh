package hust.soict.globalict.aims.utils.MyDate;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Scanner;
public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        LocalDate date = LocalDate.now();
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date){
        LocalDate d = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        this.day = d.getDayOfMonth();
        this.month = d.getMonthValue();
        this.year = d.getYear();
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if(day>0 && day <= 31)
            this.day = day;
        else
            System.out.println("Invalid day");
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month>0 && month < 13)
            this.month = month;
        else
            System.out.println("Invalid month");
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void accept(){
        System.out.println("Enter the date with the format: \"yyyy-mm-dd\" with y is year, m is month, d is day");
        Scanner in = new Scanner(System.in);
        String date = in.nextLine();
        LocalDate d = LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
        this.day = d.getDayOfMonth();
        this.month = d.getMonthValue();
        this.year = d.getYear();
    }

    public void print(){
        System.out.println("The date is " + day + "/" + month + "/" + year);
    }
}

