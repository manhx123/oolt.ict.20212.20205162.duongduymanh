package hust.soict.globalict.aims.media;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class DigitalVideoDisc extends Media{
    private String director;
    private int length;

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }
    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
    public boolean search(String title){
        List<String> thisTitle = new ArrayList<>(Arrays.asList(this.getTitle().split(" ")));
        String[] checkTitle = title.split(" ");
        for (String token: checkTitle) {
            if(!thisTitle.contains(token)){
                return false;
            }
        }
        return true;
    }
}
