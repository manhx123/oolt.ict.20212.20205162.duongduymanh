package hust.soict.globalict.aims.media;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import hust.soict.globalict.aims.Playable;
public class DigitalVideoDisc extends Disc implements Playable{
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
    public DigitalVideoDisc(String title){
        super(title);
    }
    public DigitalVideoDisc(String title,String category){
        super(title,category);
    }
    public DigitalVideoDisc(String title,String category,float cost,int length,String director){
        super(title,category,cost,length,director);
    }
    public boolean search(String title){
        String [] token = title.split(" ");
        int n= token.length;
        for(int i= 0 ;i<n;i++){
            if(this.getTitle().contains(token[i])==false)
                return false;
        }
        return true;
    }
}
