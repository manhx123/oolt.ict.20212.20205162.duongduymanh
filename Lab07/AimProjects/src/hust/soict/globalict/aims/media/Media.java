package hust.soict.globalict.aims.media;

public abstract class Media {
    private String title;
    private String category;
    private float cost;
    private int Id;

    public Media(String title , String category){
        this.title=title;
        this.category= category;
    }
    public Media(String title , String category,Float cost){
        this.title=title;
        this.category= category;
        this.cost=cost;
    }

    public String getTitle() {
        return title;
    }
    public int getId() {
        return Id;
    }
    public String getCategory() {
        return category;
    }
    public float getCost() {
        return cost;
    }
    public Media(String title){
        this.title= title;
    }
}
