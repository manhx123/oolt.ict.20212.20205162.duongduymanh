package hust.soict.globalict.aims.order.Order;
import java.util.*;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders =0;
    private static int qtyOrdered=0;

    private MyDate dateOrdered = new MyDate();
    private List<Media> itemOrdered = new ArrayList<Media>();

    public void addMedia(Media item){
        if(qtyOrdered==MAX_NUMBERS_ORDERED){
            System.out.println("The order is full\n");
            return;
        }
        if(itemOrdered.contains(item))System.out.println("This item already exists");
        else{
            itemOrdered.add(item);
            qtyOrdered++;
        }
    }

    public void addMedia(Media ... items){
        if(qtyOrdered==MAX_NUMBERS_ORDERED){
            System.out.println("The order is full\n");
            return;
        }
        if(qtyOrdered+items.length>=MAX_NUMBERS_ORDERED){
            System.out.println("The number of items adding exceeded the number of order\n");
            return;
        }
        for(Media i :items){
            if(itemOrdered.contains(i))System.out.println("This item already exists");
            else{
                itemOrdered.add(i);
                qtyOrdered++;
            }
        }
    }

    public void removeMedia(Media item){
        if(qtyOrdered==0){
            System.out.println("The order is empty");
            return;
        }
        if(itemOrdered.remove(item))qtyOrdered--;
        System.out.println("Item not found\n");
    }

    public static boolean update(){
        if(nbOrders==MAX_LIMITED_ORDERS){
            System.out.println("Maximum number of orders reached");
            return false;
        }
        else nbOrders++;
        return true;
    }

    public void print(){
        int u=0;
        System.out.println("***********************Order***********************\n");
        System.out.println("Date:");dateOrdered.printDay();
        System.out.println(nbOrders+"\n");
        System.out.println("\nOrdered items:\n");
        for(Media i: itemOrdered){
            u++;
            System.out.println(u+"."+i.getTitle()+"-"+i.getCategory()+"-"+i.getId()+"-"+i.getCost()+"$\n");
        }
        System.out.println("Total cost:"+totalCost()+"$\n");
        System.out.println("***************************************************\n");
        u=0;
        System.out.println("\nOrdered items after sorted:\n");
        Collections.sort(itemOrdered);
        for(Media i: itemOrdered){
            u++;
            System.out.println(u+"."+i.getTitle()+"-"+i.getCategory()+"-"+i.getId()+"-"+i.getCost()+"$\n");
        }
        System.out.println("***************************************************\n");
    }

    public float totalCost(){
        float total=0;
        for(Media i:itemOrdered)total=total+i.getCost();
        return total;
    }

    public void deleteById(int id){
        int num =0;
        for(Media i : itemOrdered){
            if(i.getId()==id){
                itemOrdered.remove(num);
                System.out.println("Item deleted");
                return;
            }
            num++;
        }
        System.out.println("The item is not in your order");
    }

    /*public boolean findName(String title){
        for(Media i:itemOrdered){
            if(i.search(title)==true)return true;
        }
        return false;
    }*/
}
