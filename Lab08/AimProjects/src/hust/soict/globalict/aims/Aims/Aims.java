package hust.soict.globalict.aims.Aims;
import java.util.*;
import hust.soict.globalict.aims.MemoryDaemon;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order.Order;

public class Aims {
    private static Order [] anOrder= new Order[5];
    private static int curOrder=0;

    public static void main(String[] args){
        MemoryDaemon obj = new MemoryDaemon();
        Thread newThread = new Thread(obj);
        newThread.setDaemon(true);
        newThread.start();
        Scanner input = new Scanner(System.in);
        int inp;
        while(true){
            System.out.println("Order Management Application: ");
            System.out.println("--------------------------------");
            System.out.println("1. Create new order");
            System.out.println("2. Add item to the order");
            System.out.println("3. Delete item by id");
            System.out.println("4. Display the items list of order");
            System.out.println("0. Exit");
            System.out.println("--------------------------------");
            System.out.println("Please choose a number: 0-1-2-3-4");
            System.out.print("Enter option:");
            inp=input.nextInt();
            if(inp==0){
                System.out.println("Closing system\n");
                break;
            }
            else if(inp == 1){
                if(Order.update()){
                    curOrder++;
                    anOrder[curOrder-1]=new Order();
                    System.out.println("Order created");
                }
                else System.out.println("Limit number of orders reached");
            }
            else if(inp ==2){
                System.out.println("Which item do you want to add(DVD,CD,Book):");
                String choice = input.next();
                System.out.println("Enter title:");
                String title =input.next();
                System.out.println("Enter category:");
                String category= input.next();
                System.out.println("Enter cost:");
                Float cost = input.nextFloat();
                System.out.println("Enter id:");
                int id = input.nextInt();
                if(choice.equals("DVD"))addDVD(title,category,cost,id,input);
                else if(choice.equals("CD"))addCD(title,category,cost,id,input);
                else if(choice.equals("Book"))addBook(title,category,cost,id,input);
            }
            else if(inp ==3){
                System.out.println("Enter the id of the item to delete:");
                int del= input.nextInt();
                anOrder[curOrder-1].deleteById(del);
            }
            else if(inp==4){
                anOrder[curOrder-1].print();
            }

        }
        input.close();
    }
    private static void addBook(String title,String category,float cost,int id,Scanner input){
        System.out.println("How many authors wrote this book:");
        int numAuthor=input.nextInt();
        List <String> authors= new ArrayList<String>();
        for(int i=0;i<numAuthor;i++){
            System.out.println("Enter the author "+i);
            authors.add(input.next());
        }
        Book item = new Book(title,category,cost,id,authors);
        anOrder[curOrder-1].addMedia(item);
    }

    private static void addDVD(String title,String category,float cost,int id,Scanner input){
        System.out.println("Enter the director:");
        String director= input.next();
        System.out.println("Enter length:");
        int length= input.nextInt();
        DigitalVideoDisc item = new DigitalVideoDisc(title,category,cost,id,length,director);
        anOrder[curOrder-1].addMedia(item);
        System.out.println("Would you like to play the item you just added?(Yes or No)");
        if(input.next().equals("Yes"))item.play();
    }

    private static void addCD(String title,String category,float cost,int id,Scanner input){
        System.out.println("Enter the director:");
        String director= input.next();
        System.out.println("Enter length:");
        int length= input.nextInt();
        System.out.println("Enter the artist:");
        String artist= input.next();
        CompactDisc item = new CompactDisc(title, category, cost,id, length, director,artist);
        System.out.println("How many tracks do you want to add:");
        int numTrack=input.nextInt();
        for(int i =0;i<numTrack;i++){
            System.out.println("Enter the title of track "+ i );
            String name = input.next();
            System.out.println("Enter the length of the track "+ i );
            int songLength= input.nextInt();
            item.addTrack(name, songLength);
        }
        System.out.println("Would you like to play the item you just added?(Yes or No)");
        if(input.next().equals("Yes"))item.play();
        anOrder[curOrder-1].addMedia(item);
    }

}