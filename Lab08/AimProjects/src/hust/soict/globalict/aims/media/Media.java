package hust.soict.globalict.aims.media;

public abstract class Media implements Comparable<Media> {
    private String title;
    private String category;
    private float cost;
    private int Id;
    public String getTitle() {
        return title;
    }
    public int getId() {
        return Id;
    }
    public String getCategory() {
        return category;
    }
    public float getCost() {
        return cost;
    }
    public Media(String title){
        this.title= title;
    }
    public Media(String title , String category){
        this.title=title;
        this.category= category;
    }
    public Media(String title , String category,Float cost,int Id){
        this.title=title;
        this.category= category;
        this.cost=cost;
        this.Id=Id;
    }
    public boolean equals(Media item){
        if(this.Id==item.getId())return true;
        return false;
    }
    public int compareTo(Media item){
        if(this.cost>item.getId())return 1;
        else if (this.cost<item.getId())return -1;
        else {
            if(this.cost>item.getCost())return 1;
            else if (this.cost<item.getCost())return -1;
            else return 0;
        }
    }
}
