package hust.soict.globalict.aims.media;
import java.util.*;
import hust.soict.globalict.aims.Playable;

public class CompactDisc extends Disc implements Playable /*, Comparable<CompactDisc>*/{
    private String artist;
    private List <Track> tracks = new ArrayList<Track>();
    public String getArtist() {
        return artist;
    }
    public void addTrack(String title,int length){
        Track music = new Track(title,length);
        if(tracks.contains(music))System.out.println("This tracks already exists");
        else{
            tracks.add(music);
            System.out.println("Track added");
        }
    }
    public void removeTrack(String title){
        for(Track i : tracks){
            if(i.getTitle().equals(title)){
                tracks.remove(i);
                System.out.println("Track removed");
            }
        }
        System.out.println("Track not found");
    }
    public int getLength(){
        int sumLength=0;
        for(Track i : tracks)sumLength=sumLength+ i.getLength();
        return sumLength;
    }
    public void play(){
        System.out.println("Playing: "+ this.getTitle());
        System.out.println("Length of CD: "+ getLength());
        for(Track i : tracks)i.play();
    }
    public CompactDisc(String title){
        super(title);
    }
    public CompactDisc(String title, String category){
        super(title,category);
    }
    public CompactDisc(String title,String category,float cost,int id,int length,String director,String artist){
        super(title, category, cost,id, length, director);
        this.artist=artist;
    }
   /* public int compareTo(CompactDisc item){
        if(this.tracks.size()>item.tracks.size())return 1;
        else if (this.tracks.size()<item.tracks.size())return -1;
        else {
            if(this.getLength()>item.getLength())return 1;
            else if(this.getLength()<item.getLength())return -1;
            else return 0;
        }
    }*/
}
